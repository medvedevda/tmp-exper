package hello;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.*;
        
import java.io.IOException;
import java.text.MessageFormat;
import java.util.Calendar;
import java.util.stream.Collectors;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import org.springframework.http.*;
import org.springframework.web.bind.annotation.RequestMapping;  //
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;    //
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.*; // HttpServletResponse

@Controller
@RequestMapping("/test")
public class HelloController {

    private static final String JNLP_FILE = "./stub.jnlp";
    private static final String TYPE_JNLP="application/x-java-jnlp-file";
    
    @RequestMapping("/sidec")
    public @ResponseBody
    String index() {
        return "Greetings from Side Spring Boot!";
    }
/*
    @RequestMapping(value="/jnlp", method = RequestMethod.GET, produces = "application/x-java-jnlp-file")
    public @ResponseBody
        ResponseEntity<String> jnlp(@RequestParam(required = false)) throws IOException {
            //return Files.readAllLines( Paths.get("./stub.jnlp") );
            return ResponseEntity.ok().body("STUB");
        }
*/    
    
/*
    @RequestMapping(value = "/{fileID}", method = RequestMethod.GET)
    public void getFile(
        @PathVariable("fileID") String fileName, 
        HttpServletResponse response) throws IOException {
            String src = DestLocation.concat("./"+fileName);
            InputStream is = new FileInputStream(src);
            IOUtils.copy(is, response.getOutputStream());
            response.flushBuffer();
    }
*/    
    // https://stackoverflow.com/questions/16652760/return-generated-pdf-using-spring-mvc
    @RequestMapping(value="/blob")
    public ResponseEntity<byte[]> getFile() throws IOException {
        // https://stackoverflow.com/questions/858980/file-to-byte-in-java
        Path path = Paths.get("./stub.jnlp");
        byte[] data = Files.readAllBytes(path);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.parseMediaType(TYPE_JNLP));
/*
        String filename = "stub.jnlp";
        headers.setContentDispositionFormData(filename, filename);
        headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
*/
        return new ResponseEntity<byte[]>(data, headers, HttpStatus.OK);
    }
    private static final String TEMPLATE_FILE = "./stub-template.jnlp";

    @RequestMapping(value="/jnlp")
    public ResponseEntity<String> getFileTemplate(@RequestParam("id") String id) throws IOException {
        Object[] args = {id};
        Path path = Paths.get("./stub-template.jnlp");
        String template = String.join("\n", Files.readAllLines(path));

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.parseMediaType(TYPE_JNLP));
        headers.setLastModified(Calendar.getInstance().getTimeInMillis());

        System.out.println("Template: [\n" + MessageFormat.format(template, id) + "\n]\n");
        return new ResponseEntity<String>(MessageFormat.format(template, args), headers, HttpStatus.OK);
    }

    // https://stackoverflow.com/a/46613809/6769234
    private String getResourceFileAsString(String resourceFileName) {
        InputStream is = getClass().getClassLoader().getResourceAsStream(resourceFileName);
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        return reader.lines().collect(Collectors.joining("\n"));
    }
}
